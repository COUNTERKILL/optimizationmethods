#include <math.h>
#include <iostream>
#include <iomanip>

typedef double (*func_ptr)(double*);  // double* - ��������� �� ������ , ���������� �������� ���������� �������
typedef double (*funcP_ptr)(func_ptr*, double*, double); // func-ptr* ��������� �� ������ � ���������-��������������

const int var_count = 2; // � ���������� var_count �������� ���������� ���������� �������.
const int c_count = 1; // ���������� �������-�������������

bool Restrict(func_ptr *c, double* vars)
{
	bool res = true;
	for(size_t i = 0; i < c_count; i++)
	{
		if(c[i](vars) < 0)
			res = false;
	}
	return res;
}

double GoldenSection(func_ptr f, funcP_ptr P, func_ptr *c, double r, double* vars, int var_index, double eps, double a, double b, int max_steps_count)
{
	double res = 0.0; // ���������, ������������ ��������
	double phi = (1 + sqrt(5.0)) / 2.0; // ���������� ����������� �������� �������
	double A = 0.0f, B = 0.0f; // ��� ����� ��������� �������� ������� � �������������� ������ ����������
	double x1 = a + (b - a)/phi, x2 = b - (b - a)/phi; // �������������� ����� ���������� � ����� ������������ �� �� ������� [a, b]

	int step = 0; // ������� ���

	while((b - a > eps)) // ��������� ���� �� ��� ���, ���� �������� ������ �������� �� ����� ������ �������� �����������
	{
		x1 = b - ((b - a) / phi);  // ������������� �������������� ����� ����������
		//if(Restrict(c, vars))
			vars[var_index] = x1; // �������� �� � ������ ���������� ������� �������
		A = f(vars) + P(c, vars, r); // ������������� �������� ������� � ���� �����
		// ����� ����������� �������� ��� ����� ������������ x1
		x2 = a + ((b - a) / phi);	
		//if(Restrict(c, vars))
			vars[var_index] = x2;
		B = f(vars) + P(c, vars, r);
		// ������ �������� ����������������
		if(A > B)
			a = x1;
		else
			b = x2;

		step++;
		if(step > max_steps_count)
			break;
	}

	res = (a + b) / 2;
	return res;
}

double DescentMethod(func_ptr f, funcP_ptr P, func_ptr *c, double r, double* vars, double eps, int max_steps_count)
{
	double B = f(vars) + P(c, vars, r), A = 0; // �������� ������� ������� �� ������� � ���������� ���������
	bool was_counted = false; // ��� �� ��������� ������� �� ����������� ���������� ���������� ��������
	int stpes_ellapsed = 0; // ���������� ��������, ����������� �� ����� �������� (�������������� ����������)
	double delta = 0.0; // ���������� ����������� (�������������� ����������)
	for(int i = 0; i < max_steps_count; i++){
		A = B; // ���������� �������� ������� �������

		for(int var_index = 0; var_index < var_count; var_index++) // �������� �� ������� ��������� ������� �������
			vars[var_index] = GoldenSection(f, P, c, r, vars, var_index, eps, -5000, 5000, max_steps_count); // � ������� ������� ���������� ������� �� ���� ��

		B = f(vars) + P(c, vars, r); // ������� �������� ������� �������

		delta = fabs(A - B);

		if(delta <= eps)
		{
			stpes_ellapsed = i + 1;
			was_counted = true;
			break;
		}
	}
	return f(vars) + P(c, vars, r);
}


// x > 3
double c1(double* x)
{
	return x[0]-3;
}

// ����� �������� �������
double P(func_ptr *c, double *vars, double r)
{
	double summ = 0;
	for(size_t i = 0; i < c_count; i++)
	{
		//summ+=1/c[i](vars);
		summ+=(c[i](vars)>0)?-log(c[i](vars)):99999999999999999;
	}
	summ*=r;
	return summ;
}

//---------------------------------------------------------------------------------------
// F - �������� �������,
// C - ��������� �� ������ �������-�����������
// Eps - ��������� ���������
// x0 - ��������� ���������� �����
// vars - ������, �������� �������� ���������� �������
//---------------------------------------------------------------------------------------
double InputShtrafFunctions(func_ptr f, func_ptr *c,  double* vars, double eps)
{
	double r = 100; 
	double b = 0.5; // 0 < b < 1
	double res = 0;
	while(fabs(P(c, vars, r)) >= eps)
	{
		res = DescentMethod(f, P, c, r, vars, eps, 10000000000000000);
		r = r * b;
	}
	std::cout << std::setprecision(5) << vars[0] << std::endl << vars[1];
	return res;
}

double function(double* variables)
{
	return (variables[0]- 2)*(variables[0] - 2) + (variables[1]- 2)*(variables[1] - 2);
}



int main(int argc, char* argv[])
{
	setlocale(LC_ALL,"RUSSIAN");

	double eps = 0.0;
	int max_steps_count = 0;
	double variables[var_count] = {0.0};
	func_ptr c[1] = {0};
	c[0] = c1;
	InputShtrafFunctions(function,c, variables, 0.0001);

	return 0;
}



